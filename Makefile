SPECFILE            = $(shell find -maxdepth 1 -type f -name '*.spec')
SPECFILE_NAME       = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION    = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
SPECFILE_RELEASE    = $(shell awk '$$1 == "Release:"  { print $$2 }' $(SPECFILE) )
TARFILE             = $(SPECFILE_NAME)-$(SPECFILE_VERSION).tgz
DIST               ?= $(shell rpm --eval %{dist})

clean:
	rm -rf build/ $(TARFILE)

rpm:
	rpmbuild -bb --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' --define 'kernvers 4.18.0-80.11.2.el8_0' $(SPECFILE)

srpm:
	rpmbuild -bs --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' --define 'kernvers 4.18.0-80.11.2.el8_0' $(SPECFILE)

