# openafs

## Overview

* This repository builds openafs client RPMs via the standard rpmci logic
* In addition this repository supports the building of kmod-openafs packages by passing "$BUILD_KMOD" as a gitlab-ci variable. This is most likely useful via a pipeline schedule (and is currently configured to run daily at 09:00)

## Build specifics

* openafs.spec by default does not build the kmod packages (%define build_modules 0)
* kmod packages are built via rpmci, using a custom script (build_kmod.sh) and custom stages (executed through the variable $BUILD_KMOD)
* the package name for 'kmod-openafs' in koji is 'openafs'. To ensure that we can maintain previous uniquely named builds for each kmod kernel version, the spec file is massaged to include the kernel release in the Release string of spec. The resulting version in koji looks like https://koji.cern.ch/buildinfo?buildID=39307
* As all of our kmod packages are defined in koji as the name 'openafs', the 'openafs8-testing' repo only contain the latest build (due to mash configuration). For this reason, all kmod-packages are automatically promoted to 'openafs8-qa'(http://linuxsoft.cern.ch/internal/repos/openafs8-qa/x86_64/os/Packages/)
* build_kmod.sh is the script responsible for building kmod-openafs. This script has two arguments which are called through the .gitlab-ci.yml
    * 'srpm': determine the latest kernel version available, determine if a kmod for this kernel does not currently exist in 'openafs8-qa', tweak the spec to build for the kernel version, build srpm
    * 'rpm': install kernel-devel for the kernel we are building against, build rpm from src.rpm above
    * kmod_koji_build8 (extension of rpmci koji_build) uses the srpm created above
* openafs upstream uses a helper script to generate the kmod-openafs spec file. This file (src/openafs-kmodtool) has been modified to support our custom koji massages

## new OpenAFS upstream releases
* a helper script `new_upstream_release.sh` will download the new tarball(s) and fix up hardcoded references to the version. The result will still need to be manually committed back to GIT.
* **commit to a new branch first, don't merge to master until the package is fully tested and ready to be deployed**. Kmods will be built based off of what is in master.

## Building KMODs locally for testing

The best way of testing the build of a KMOD for a particular OS is to run the docker image locally.
For example, here's what you'd do to test the KMOD for CS9:

```bash
$ docker run --rm -it gitlab-registry.cern.ch/linuxsupport/rpmci/builder-cs9
[root@1f9db62a4463 /]# git clone https://:@gitlab.cern.ch:8443/linuxsupport/rpms/openafs.git
[root@1f9db62a4463 /]# cd openafs/
[root@1f9db62a4463 openafs]# git checkout v189pre23
[root@1f9db62a4463 openafs]# export KOJI_TAG=openafs
[root@1f9db62a4463 openafs]# export DIST='.el9.cern'
[root@1f9db62a4463 openafs]# ./build_kmod.sh srpm
[root@1f9db62a4463 openafs]# ./build_kmod.sh rpm
```

Obviously, you'll have to select the correct docker image, the branch of openafs you want to test
and the correct `DIST` tag you're building for.
